import { Action, Mutation } from './types'
import api from '@/api/products';

const actions = {
  async [Action.GET_CATEGORIES] ({ commit }) {
    const categories = await api.getCategories();
    const categoriesFlat = await api.getCategoriesFlat();
    commit(`${Mutation.SET_CATEGORIES}`, {
      categories: categories.data,
      categoriesFlat: categoriesFlat.data,
    });
  },

  async [Action.GET_COMPANIES] ({ commit }) {
    const companies = await api.getCompanies();
    commit(`${Mutation.SET_COMPANIES}`, companies.data);
  },

  async [Action.GET_STORE] ({ state, commit }, storeId) {
    try {
      const store = await api.getStore(storeId)

      if (store.error) {
        commit(`${Mutation.ERROR_SERVER_RESPONSE}`);
      } else {
        commit(`${Mutation.SET_STORE}`, store.data);
      }
    } catch (error) {
      let currentError = null;

      if (error.response) {
        currentError = error.reponse.data;
      } else if (error.request) {
        currentError = error.request
      } else {
        currentError = error.message;
      }
      commit(`${Mutation.ERROR_SERVER_RESPONSE}`, currentError);
    }
  },

  async [Action.GET_STORES_TOP] ({ state, commit }) {
    try {
      const top = await api.getStoreTop()

      if (top.error) {
        commit(`${Mutation.ERROR_SERVER_RESPONSE}`);
      } else {
        commit(`${Mutation.SET_STORES_TOP}`, top.data);
      }
    } catch (error) {
      let currentError = null;

      if (error.response) {
        currentError = error.reponse.data;
      } else if (error.request) {
        currentError = error.request
      } else {
        currentError = error.message;
      }
      commit(`${Mutation.ERROR_SERVER_RESPONSE}`, currentError);
    }
  },

  async [Action.GET_ALL_PRODUCTS] ({ state, commit }, payload) {

    commit(`${Mutation.START_LOADING}`);
    let products = [];

    if (state.storeId !== null) {
      products = await api.getStoreProducts(state.storeId, payload)
    } else {
      products = await api.getProducts(payload)
    }

    if (products.error) {
      commit(`${Mutation.ERROR_SERVER_RESPONSE}`);
    } else {
      commit(`${Mutation.SET_PRODUCTS}`, products.data)
    }

  },

  async [Action.GET_PRODUCT] ({ state, commit }, id) {
    try {
      commit(`${Mutation.START_LOADING}`);
      const product = await api.getProductById(id);
      commit(`${Mutation.STOP_LOADING}`);

      if (product.error) {
        commit(`${Mutation.ERROR_SERVER_RESPONSE}`);
      } else if (product.data) {
        commit(`${Mutation.SET_PRODUCT}`, product.data);
      }

    } catch (error) {
      commit(`${Mutation.STOP_LOADING}`);
      let currentError = null;
      if (error.response) {
        currentError = error.reponse.data;
      } else if (error.request) {
        currentError = error.request
      } else {
        currentError = error.message;
      }
      commit(`${Mutation.ERROR_SERVER_RESPONSE}`, currentError);
    }

  },

  async [Action.SEND_CART_TO_SERVER] ({ state, commit }, payload) {
    const cart = state.cart;

    try {
      commit(`${Mutation.INITIAL_SENDING_ORDER}`);

      const params = {
        products: cart.products,
        email: payload.email,
        phone: payload.phone,
        comment: payload.comment
      }

      const response = await api.sendCartToServer(params);

      commit(`${Mutation.FINISH_SENDING_ORDER}`);

      if (response.error) {
        commit(`${Mutation.ERROR_SERVER_RESPONSE}`, error);
      } else {
        commit(`${Mutation.NOTICE_SERVER_RESPONSE}`, response.data.message)
        commit(`${Mutation.SUCCESS_SEND_CART}`)
      }
    } catch (error) {
      let currentError = '';
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        currentError = error.reponse.data;
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        currentError = error.request
      } else {
        // Something happened in setting up the request that triggered an Error
        currentError = error.message;
      }

      commit(`${Mutation.ERROR_SERVER_RESPONSE}`, currentError);
    }
  },
}

export default actions;
