import { endpoint, http } from '@/utils/http';

export default {
  getCategories (params = {}) {
    return http.get(endpoint + 'api/admin/v1/json/categories', { params: params})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getCategoriesFlat (params = {}) {
    return http.get(endpoint + 'api/admin/v1/json/categoriesflat', { params: params})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getCompanies (params = {}) {
    return http.get(endpoint + 'api/v1/json/products/companies/getall', { params: params})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getStore (storeId) {
    return http.get(endpoint + `api/v1/json/store/get/${storeId}`, {})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getStoreTop () {
    return http.get(endpoint + `api/v1/json/stores/stat`, {})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getProducts (params = {}) {
    return http.get(endpoint + 'api/v1/json/products', { params: params})
      .then(response => {

         return response;
      })
      .catch(error => {

        return {
          "error": error
        };
      })
  },

  getStoreProducts (storeId, params = {}) {
    return http.get(endpoint + `api/v1/json/store/${storeId}/products`, { params: params})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  getProductById (id) {
    return http.get(endpoint + 'api/v1/json/product/get/' + id, {})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

  sendCartToServer (params = {}) {
    return http.post(endpoint + 'api/v1/json/product/buy', { params: params})
      .then(response => {
         return response;
      })
      .catch(error => {
        return {
          "error": error
        };
      })
  },

}
