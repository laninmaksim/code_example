<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;

use App\Repositories\WatchListRepository;

use App\Http\Requests\Watchlist\GetByUUIDRequest;
use App\Http\Requests\Watchlist\SaveRequest;

use App\Http\Resources\WatchListResource;

class WatchlistController extends Controller
{

    /**
     * Getting WatchList by uuid.
     *
     * @param GetByUUIDRequest $request
     * @return JsonResponse
     */
    public function get(GetByUUIDRequest $request): JsonResponse
    {
        $watchList = app(WatchListRepository::class)->getByUUID($request->input('uuid'));

        if (count($watchList) > 0) {
            return response()->json()
                ->success()
                ->with(new WatchListResource($watchList[0]));
        }
            
        return response()->json()
            ->error(0, [
                'error' => 'not found watch list',
                'message' => 'Merkliste nicht gefunden'
            ], 404);
    }

    /**
     * Save WatchList to DB.
     *
     * @param SaveRequest $request
     * @return JsonResponse
     */
    public function save(SaveRequest $request): JsonResponse
    {
        try {
            $watchList = app(WatchListRepository::class)->save($request->validated());

            return response()->json()
                ->success()
                ->with([ 
                    'message' => 'watch list saved successfully',
                    'uuid' => $watchList->uuid,
                ]);
        } catch (\Throwable $th) {
            return response()->json()
                ->error(2, [ 
                    'message' => 'error when saving watch list...',
                    'error' => $th,
                ]);
        }
    }

}
