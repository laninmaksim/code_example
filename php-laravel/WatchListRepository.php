<?php

namespace App\Repositories;

use App\Models\WatchList;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Prettus\Repository\Eloquent\BaseRepository;

class WatchListRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return WatchList::class;
    }

    /**
     * Get watch list by uuid.
     *
     * @param  string  $uuid
     * @return Collection
     */
    public function getByUUID(string $uuid): Collection
    {
        return $this->model::query()
            ->where('uuid', $uuid)
            ->get();
    }

    /**
     * New watch list OR save by uuid.
     *
     * @param  array  $data
     * @return WatchList
     */
    public function save(array $data): WatchList
    {
        if (isset($data['uuid']) && $data['uuid'] !== '') {
            $watchList = $this->getByUUID($data['uuid']);
        } else {
            $watchList = new WatchList();
            $watchList->uuid = (string) Str::uuid();
        }

        $watchList->list = $data['list'];
        $watchList->save();

        return $watchList;
    }
}
