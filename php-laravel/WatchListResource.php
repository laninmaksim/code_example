<?php

namespace App\Http\Resources;

use App\Models\WatchList;
use Illuminate\Http\Resources\Json\JsonResource;

class WatchListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var WatchList $this */
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'list' => $this->list,
        ];
    }
}
